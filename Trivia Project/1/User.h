#pragma once

#include "Includes.h"

class TriviaServer;
class Room;
class Game;

class User
{
public:
	User(string, SOCKET);
	~User();
	Room* getRoom();
	void send(string);
	void setGame(Game *);
	Game * getGame();
	void clearGame();
	bool createRoom(int, string, int, int, int); // int roomId, string roomName, int maxUsers, int questionsNo, int questionTime
	bool joinRoom(Room *);
	void leaveRoom();
	int closeRoom();
	bool leaveGame();
	void clearRoom();

	string getUsername() { return _username; }

	
private:
	string _username;
	SOCKET _sock;
	Game * _game;
	Room * _room;

};

