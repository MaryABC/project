#include "Validator.h"


bool Validator::isPasswordValid(string pass)
{
	if (pass.length() < 4)
		return false;
	if (!(pass.find(' ') == string::npos))
		return false;
	
	bool isDigitExist = false;

	for (unsigned int i = 0; i < pass.length() && !isDigitExist; i++)
		if (pass[i] >= '0' && pass[i] <= '9')
			isDigitExist = true;

	if (!isDigitExist)
		return false;

	bool isUpperCaseExist = false;

	for (unsigned int i = 0; i < pass.length() && !isUpperCaseExist; i++)
		if (pass[i] >= 'A' && pass[i] <= 'Z')
			isUpperCaseExist = true;

	if (!isUpperCaseExist)
		return false;

	bool isLowerCaseExist = false;

	for (unsigned int i = 0; i < pass.length() && !isLowerCaseExist; i++)
		if (pass[i] >= 'a' && pass[i] <= 'z')
			isLowerCaseExist = true;

	if (!isLowerCaseExist)
		return false;

	return true;
}

bool Validator::isUsernameValid(string user)
{
	if (user.length() == 0)
		return false;
	if (!((user[0] >= 'a' && user[0] <= 'z') || (user[0] >= 'A' && user[0] <= 'Z')))
		return false;
	if (!(user.find(' ') == string::npos))
		return false;

	return true;
}
