#include "DataBase.h"

unordered_map<string, vector<string>> _results;
int callback(void* notUsed, int argc, char** argv, char** azCol);
void clearTable();
bool execDB(string, int(*callback)(void *, int, char **, char **) = NULL);

DataBase::DataBase(string dbName)
{
	if (!openDB(dbName))
		throw exception("Can't open database");
//	_results = unordered_map<string, vector<string>>();
}


DataBase::~DataBase()
{
	sqlite3_close(_db);
}

bool DataBase::isUserExist(string username)
{
	string op = "select * from t_users where username == '" + username + "';";
	if (!execDB(op, callback))
	{
		clearTable();
		return false;
	}
		
	if (_results.size() != 0)
	{
		auto iter = _results.end();
		iter--;
		int size = iter->second.size();
		auto it = _results.begin();

		if (size == 1 && it->second[0].compare(username) == 0)
		{
			clearTable();
			return true;
		}
	}
	clearTable();
	return false;
}

bool DataBase::addNewUser(string username, string password, string email)
{

	if (!execDB("BEGIN;"))
	{
//		throw _zErrMsg;
		return false;
	}

	string op = "insert into t_users(username, password, email) values('" + username + "', '" + password + "', '" + email + "');";

	if (!execDB(op))
		return false;

	if (!execDB("COMMIT;"))
		return false;

	return true;
}

bool DataBase::isUserAndPassMatch(string username, string password)
{
	string op = "select * from t_users where username == '" + username + "';";
	execDB(op, callback);

	if (_results.size() != 0)
	{
		auto iter = _results.end();
		iter--;
		int size = iter->second.size();
		auto it = _results.begin();

		if (size == 1 && it->second[0].compare(username) == 0)
		{
			it++;
			if (it->second[0].compare(password) == 0)
			{
				clearTable();
				return true;
			}
		}
	}
	clearTable();
	return false;
}

vector<Question*> DataBase::initQuestions(int questionsNo) // Works only if there is any questions !!!
{
	vector<Question*> questions;
	string op = "SELECT * FROM t_questions ORDER BY RANDOM() LIMIT " + to_string(questionsNo) + ";";

	execDB(op, callback);

	auto it = _results.begin();
	int size = it->second.size();

	int cond = (questionsNo < size) ? questionsNo : size;	// Don't know if it's necessary...
															// It won't work witout the right amount of questions
	for (int i = 0; i < cond; i++)
	{
		it = _results.begin();
		int questionId = atoi(it->second[i].c_str());
			
		it++;
		string question = it->second[i];

		it++;
		string correctAns = it->second[i];

		it++;
		string ans2 = it->second[i];

		it++;
		string ans3 = it->second[i];

		it++;
		string ans4 = it->second[i];

		Question * tmp = new Question(questionId, question, correctAns, ans2, ans3, ans4);
		questions.push_back(tmp);
	}
	clearTable();
	return questions;
}

int DataBase::insertNewGame()
{

	string op = "insert into t_games(status, start_time, end_time) values(0, 'NOW', null);";

	execDB("COMMIT;");

	if (!execDB("BEGIN;"))
		return -1;
	
	if (!execDB(op))
		return -1;

	if (!execDB("COMMIT;"))
		return -1;

	op = "select last_insert_rowid();";
	execDB(op, callback);

	string id = "-1"; // id last_insert_rowid() didn't found
	if (_results.size() != 0)
	{
		auto it = _results.begin();
		id = it->second[0];
	}

	clearTable();
	return atoi(id.c_str());
}

bool DataBase::updateGameStatus(int gameId)
{
	string op = "update t_games set status = 1, end_time = 'NOW' where game_id == " + to_string(gameId) + ";";

	if (!execDB("BEGIN;"))
		return false;

	if (!execDB(op)) // error
		return false;

	if (!execDB("COMMIT;"))
		return false;

	return true;
}

bool DataBase::addAnswerToPlayer(int gameId, string username, int questionId, string answer, bool isCorrect, int answerTime)
{
	string isCor = isCorrect ? "1" : "0";
	string op = "insert into t_players_answers(game_id, username, question_id, player_answer, is_correct, answer_time) ";
	op += "values('" + to_string(gameId) + "', '" + username + "', '" + to_string(questionId) + "', '" + answer + "', '";
	op += isCor + "', '" + to_string(answerTime) + "');";

	if (!execDB("BEGIN;"))
		return false;

	if (!execDB(op))
		return false;

	if (!execDB("COMMIT;"))
		return false;

	return true;
}

vector<string> DataBase::getPersonalStatus(string username)
{
	vector <string> values;

	string op = "select count() from t_players_answers where username == '" + username + "';";
	if (!execDB(op, callback))
		return values;

	auto it = _results.begin();
	string gamesNum = it->second[0];

	int right = 0;
	int wrong = 0;
	float time = 0;

	int size = stoi(gamesNum.c_str());
	for (int i = 0; i < size; i++)
	{
		clearTable();

		op = "select question_id, player_answer, answer_time from t_players_answers where username == '" + username + "';";
		if (!execDB(op, callback))
			return values;

		it = _results.begin();
		string questionId = it->second[i];
		it++;
		string playerAnswer = it->second[i];
		it++;
		time += stoi(it->second[i].c_str());

		clearTable();

		op = "select correct_ans from t_questions where question_id == '" + questionId + "';";
		if (!execDB(op, callback))
			return values;

		it = _results.begin();
		string correctAnswer = it->second[0];

		if (playerAnswer == correctAnswer)
			right++;
		else
			wrong++;
	}

	clearTable();

	time /= size;
	int shlemim = int(time);
	int shvarim = int((time - shlemim) * 100);

	values.push_back(gamesNum);
	values.push_back(to_string(right));
	values.push_back(to_string(wrong));
	values.push_back(to_string(shlemim));
	values.push_back(to_string(shvarim));

	return values; // Number of games, Number of right answers, Number of wrong answers, Shlemim, Shvarim
}

vector<string> DataBase::getBestScores()
{
	vector <string> values;
	vector <string> tmpStatus;
	multimap<int, string> tmp;

	string op = "select count() from t_users;";
	if (!execDB(op, callback))
		return values;

	auto it = _results.begin();
	int usersNumber = stoi(it->second[0].c_str());

	for (int i = 0; i < usersNumber; i++)
	{
		clearTable();

		string op = "select username from t_users";
		if (!execDB(op, callback))
			return values;

		it = _results.begin();
		string username = it->second[i];
		clearTable();
		tmpStatus = getPersonalStatus(username);
		int score = stoi(tmpStatus[1].c_str());

		pair <int, string> tmpPair(score, username);
		tmp.insert(tmpPair);
	}

	auto iter = tmp.end();
	iter--;

	values.push_back(to_string(iter->second.size()));
	values.push_back(iter->second);
	values.push_back(to_string(iter->first));

	iter--;

	values.push_back(to_string(iter->second.size()));
	values.push_back(iter->second);
	values.push_back(to_string(iter->first));

	iter--;

	values.push_back(to_string(iter->second.size()));
	values.push_back(iter->second);
	values.push_back(to_string(iter->first));

	return values; // firstNameSize, firstName, firstScore, secondNameSize, secondName, secondScore, thirdNameSize, thirdName, thirdScore, 
	
}

int DataBase::callbackCount(void *, int, char **, char **)
{
	return 0;
}

int DataBase::callbackQuestions(void *, int, char **, char **)
{
	return 0;
}

int DataBase::callbackBestScores(void *, int, char **, char **)
{
	return 0;
}

int DataBase::callbackPersonalStatus(void *, int, char **, char **)
{
	return 0;
}

int callback(void* notUsed, int argc, char** argv, char** azCol)
{
	int i;

	for (i = 0; i < argc; i++)
	{
		auto it = _results.find(azCol[i]);
		if (it != _results.end())
		{
			it->second.push_back(argv[i]);
		}
		else
		{
			pair<string, vector<string>> p;
			p.first = azCol[i];
			p.second.push_back(argv[i]);
			_results.insert(p);
		}
	}

	return 0;
}

void clearTable()
{
	for (auto it = _results.begin(); it != _results.end(); ++it)
	{
		it->second.clear();
	}
	_results.clear();
}

bool DataBase::openDB(string dbName)
{
	int rc = sqlite3_open(dbName.c_str(), &_db);
	if (rc)
	{
//		cout << "Can't open database: " << sqlite3_errmsg(_db) << endl;
		sqlite3_close(_db);
		return false;
	}
	return true;
}

bool DataBase::execDB(string op, int(*callback)(void *, int, char **, char **))
{
	int rc = sqlite3_exec(_db, op.c_str(), callback, 0, &_zErrMsg);
	if (rc != SQLITE_OK)
	{
		//		cout << "SQL error: " << _zErrMsg << endl;
		sqlite3_free(_zErrMsg);
		return false;
	}
	return true;
}


/**bool DataBase::execDB(string op, int(*callback)(void *, int, char **, char **))
{
	int rc = sqlite3_exec(_db, op.c_str(), callback, 0, &_zErrMsg);
	if (rc != SQLITE_OK)
	{
//		cout << "SQL error: " << _zErrMsg << endl;
		sqlite3_free(_zErrMsg);
		return false;
	}
	return true;
}**/
