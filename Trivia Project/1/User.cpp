#include "User.h"

User::User(string username, SOCKET sock) : _username(username), _sock(sock), /*_game(nullptr),*/ _room(nullptr)
{
}

User::~User()
{
}

Room * User::getRoom()
{
	return _room;
}

void User::send(string message)
{
	Helper::sendData(_sock, message);
}

void User::setGame(Game * game)
{
	_game = game;
	_room = nullptr;
}

Game * User::getGame()
{
	return _game;
}

void User::clearGame()
{
	_game = nullptr;
}

bool User::createRoom(int roomId, string roomName, int maxUsers, int questionsNo, int questionTime)
{
	if (_room != nullptr)
	{
		send("1141");
		return false;
	}

	_room = new Room(roomId, this, roomName, maxUsers, questionsNo, questionTime);
	send("1140");

	return true;
}

bool User::joinRoom(Room * newRoom)
{
	if (_room != nullptr)
		return false;
	_room = newRoom;
	return newRoom->joinRoom(this);
}

void User::leaveRoom()
{
	if (_room != nullptr)
	{
		_room->leaveRoom(this);
		_room = nullptr;
	}
}

int User::closeRoom()
{
	if (_room != nullptr)
	{
		int ans = _room->closeRoom(this);
		delete _room;
		_room = nullptr;
		return ans;
	}
	return -1;
}

bool User::leaveGame() // not written
{
	return false;
}

void User::clearRoom()
{
	_room = nullptr;
}
