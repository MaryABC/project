#include "TriviaServer.h"
#include <fstream>
#define PORT 8820

TriviaServer::TriviaServer()
{
	srand(time(NULL));
	_db = new DataBase();

	_roomIdSequence = 0;

	WSADATA wsaData;

	//	int iSendResult;
	//char recvbuf[DEFAULT_BUFLEN];

	// Initialize Winsock
	int iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (iResult != 0) {
		printf("WSAStartup failed with error: %d\n", iResult);
		system("PAUSE");
		throw exception("WSAStartup failed");
	}

	_socket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (_socket == INVALID_SOCKET)
	{
		printf("soket function failed = %d\n", WSAGetLastError());
		system("PAUSE");
		throw exception("soket function failed");
	}

}


TriviaServer::~TriviaServer()
{
	//suppoused to alse delete the list of rooms and users?? (but not the rooms and users themseleves??)
	closesocket(_socket);
}

void TriviaServer::serve()
{
	bindAndListen();
	thread t(&TriviaServer::handleRecievedMessages, this);
	t.detach();
	while (true)
	{
		accept();
	}
}

void TriviaServer::bindAndListen()
{
	struct sockaddr_in serv_sock_def;
	serv_sock_def.sin_family = AF_INET;
	serv_sock_def.sin_addr.s_addr = inet_addr("0.0.0.0");
	serv_sock_def.sin_port = htons(PORT);


	int iResult = ::bind(_socket, (SOCKADDR *)&serv_sock_def, sizeof(serv_sock_def));
	if (iResult == SOCKET_ERROR) {
		wprintf(L"bind failed with error %u\n", WSAGetLastError());
		closesocket(_socket);
		WSACleanup();
		system("PAUSE");
		throw exception("bind failed");
	}

	if (listen(_socket, SOMAXCONN) == SOCKET_ERROR) {
		wprintf(L"listen failed with error: %ld\n", WSAGetLastError());
		closesocket(_socket);
		WSACleanup();
		system("PAUSE");
		throw exception("listen failed");
	}
}

void TriviaServer::accept()
{
	SOCKET client_sock = ::accept(_socket, NULL, NULL);
	if (client_sock == INVALID_SOCKET) {
		wprintf(L"accept failed with error: %ld\n", WSAGetLastError());
		closesocket(client_sock);
		WSACleanup();
		system("PAUSE");
		throw exception("accept failed");
	}
	else
	{

		try
		{
			thread t(&TriviaServer::clientHandler, this, client_sock);
			t.detach();
		}
		catch (exception e)
		{

		}
	}
}

void TriviaServer::clientHandler(SOCKET client_socket)//what errors can be caused here? need to be catched and crete and end of communication message
{
	try
	{
		int code = Helper::getMessageTypeCode(client_socket);
		while (code != 0 && code != 299 /*end_communication_code*/)
		{
			addRecievedMessage(buildRecieveMessage(client_socket, code));
			code = Helper::getMessageTypeCode(client_socket);
			if (code == 219)
			{
				code = 219;
				if (1 == 0)//bonus related
				{
					code = 228;
					Helper::getMessageTypeCode(client_socket);//to 'throw away' parameters (length is 3)
				}
			}
		}
		//add end of communication messege
		addRecievedMessage(buildRecieveMessage(client_socket, 299));
	}
	catch (exception e)
	{
		addRecievedMessage(buildRecieveMessage(client_socket, 299));
	}
}

Room * TriviaServer::getRoomById(int roomId)
{
	std::map<int, Room*>::iterator it = _roomsList.find(roomId);
	if (it != _roomsList.end())
		return it->second;
	return nullptr;
}

User * TriviaServer::getUserByName(string username)
{
	std::map<SOCKET, User*>::iterator it;
	for (it = _connectedUsers.begin(); it != _connectedUsers.end(); it++)
		if (it->second->getUsername() == username)
			return it->second;
	return nullptr;
}

User * TriviaServer::getUserBySocket(SOCKET client_socket)
{
	std::map<SOCKET, User*>::iterator it = _connectedUsers.find(client_socket);
	if (it != _connectedUsers.end())
		return it->second;
	return nullptr;
}


void TriviaServer::handleRecievedMessages()
{	
	while (true)
	{
		unique_lock<mutex> lock(_mtxRecievedMessages);

		if (_queRcvMessages.empty())
			cond.wait(lock);
		
		RecievedMessage* msg = _queRcvMessages.back();
		_queRcvMessages.pop();

		lock.unlock();
		
		msg->setUser(getUserBySocket(msg->getSock()));
		try
		{
			switch (msg->getMessageCode())
			{
			case 200:
				handleSignin(msg);
				break;

			case 203:
				handleSignup(msg);
				break;

			case 205:
				handleGetRooms(msg);
				break;

			case 207:
				handleGetUsersInRoom(msg);
				break;

			case 209:
				handleJoinRoom(msg);
				break;

			case 211:
				handleLeaveRoom(msg);
				break;

			case 213:
				handleCreateRoom(msg);
				break;

			case 215:
				handleCloseRoom(msg);
				break;

			case 217:
				handleStartGame(msg);
				break;

			case 219:
				handlePlayerAnswer(msg);
				break;

			case 222:
				handleLeaveGame(msg);
				break;

			case 223:
				handleGetBestScores(msg);
				break;

			case 225:
				handleGetPersonalStatus(msg);
				break;

			case 228:
				handleGameChat(msg);
				break;

			case 201:
				handleSignout(msg);
				break;

			default:
				safeDeleteUesr(msg);//if there is any error or exeption, do this too
			}
		}
		catch (exception e)
		{
			safeDeleteUesr(msg);
		}
		delete msg;
	}
}

void TriviaServer::safeDeleteUesr(RecievedMessage * msg)//this should catch exceptions from the inside
{
	try
	{
		SOCKET sock = msg->getSock();
		handleSignout(msg);
		closesocket(sock);
	}
	catch (exception e)
	{
		//should we do something?
	}

}

User * TriviaServer::handleSignin(RecievedMessage * msg)//needs to be updated when db is added
{
//	map<string, string> signedin_users = getUserNamesAndPasswords();

	string username = msg->getValues()[0];
	string password = msg->getValues()[1];

	if (!_db->isUserAndPassMatch(username, password))
	{
		Helper::sendData(msg->getSock(), "1021"); //wrong details code
		return nullptr;
	}
	if (getUserByName(username) != nullptr)
	{
		Helper::sendData(msg->getSock(), "1022"); //User is already connected code
		return nullptr;
	}
	User* user = new User(username, msg->getSock());
	_connectedUsers[msg->getSock()] = user;
	Helper::sendData(msg->getSock(), "1020"); //success code
	return user;
}

void TriviaServer::handleSignout(RecievedMessage * msg)
{
	if (getUserBySocket(msg->getSock()) != nullptr)
	{
		User* user = _connectedUsers.find(msg->getSock())->second;
		_connectedUsers.erase(_connectedUsers.find(msg->getSock()));
		handleCloseRoom(msg);
		handleLeaveRoom(msg);
		/*handleLeaveGame(msg); /*sould be added, but in what order?*/
		delete user;
	}
}

bool TriviaServer::handleSignup(RecievedMessage * msg)
{
	string user = msg->getValues()[0];
	string pass = msg->getValues()[1];
	string email = msg->getValues()[2];

	if (!Validator::isPasswordValid(pass))
	{
		Helper::sendData(msg->getSock(), "1041");
		return false;
	}
	if (!Validator::isUsernameValid(user))
	{
		Helper::sendData(msg->getSock(), "1043");
		return false;
	}
	if(_db->isUserExist(user))				// Add when db is added
	{
		Helper::sendData(msg->getSock(), "1042");
		return false;
	}

	// Delete when db is added //
//	map<string, string> m = getUserNamesAndPasswords();
//	if (m.find(user) == m.end())
//	{
//		Helper::sendData(msg->getSock(), "1042");
//		return false;
//	}
	/////////////////////////////

	try
	{
		_db->addNewUser(user, pass, email);
	}
	catch (exception e)
	{
		Helper::sendData(msg->getSock(), "1044");
		return false;
	}
	Helper::sendData(msg->getSock(), "1040");

	return true;
}

void TriviaServer::handleLeaveGame(RecievedMessage * msg)
{
	if (!msg->getUser()->getGame()->leaveGame(msg->getUser()))
	{
		delete msg->getUser()->getGame();
	}
}

void TriviaServer::handleStartGame(RecievedMessage * msg)
{
	User * user = msg->getUser();
	Room * room = user->getRoom();
	Game * game = nullptr;
	vector <User *> players = room->getUsers();
	try
	{
		map<int, Room*>::iterator to_erase = _roomsList.find(room->getId());
		game = new Game(players, room->getQuestionsNo(), *_db);
		game->sendFirstQuestion();
		if (to_erase != _roomsList.end())
			_roomsList.erase(to_erase);
		//user->setGame(game);
	}
	catch (exception e)
	{
		user->send("1180");
	}
	
}

void TriviaServer::handlePlayerAnswer(RecievedMessage * msg)
{
	User * user = msg->getUser();
	Game * game = user->getGame();

	int answerNo = atoi(msg->getValues()[0].c_str());
	int time = atoi(msg->getValues()[1].c_str());

	bool isRunning = true;

	if (game)
		isRunning = game->handleAnswerFromUser(user, answerNo, time);

	if (!isRunning)
		delete game;
}

void TriviaServer::handleGameChat(RecievedMessage * msg)
{
	msg->getUser()->getGame()->handleChat(msg->getUser(), msg->getValues()[0]);
}

bool TriviaServer::handleCreateRoom(RecievedMessage * msg)
{
	try {
		if (msg->getUser() != nullptr)
		{
			_roomIdSequence++;
			if (msg->getUser()->createRoom(_roomIdSequence, msg->getValues()[0], stoi(msg->getValues()[1]), stoi(msg->getValues()[2]), stoi(msg->getValues()[3])))
			{
				_roomsList[_roomIdSequence] = msg->getUser()->getRoom();
				return true;
			}
		}
	}
	catch(exception e){}
	_roomIdSequence--;
	return false;
}

bool TriviaServer::handleCloseRoom(RecievedMessage * msg)
{
	if (msg->getUser()->getRoom() == nullptr)
		return false;
	map<int, Room*>::iterator to_erase = _roomsList.find(msg->getUser()->getRoom()->getId());
	if (to_erase!=_roomsList.end() && msg->getUser()->closeRoom() != -1)
	{
		_roomsList.erase(to_erase);
		return true;
	}
	return false;
}

bool TriviaServer::handleJoinRoom(RecievedMessage * msg)
{
	if(msg->getUser()==nullptr) return false;
	if (getRoomById(stoi(msg->getValues()[0])) == nullptr) Helper::sendData(msg->getSock(), "1102"); // failed - room not exist or other reason
	return msg->getUser()->joinRoom(getRoomById(stoi(msg->getValues()[0])));
}

bool TriviaServer::handleLeaveRoom(RecievedMessage * msg)
{
	if (msg->getUser() == nullptr) return false;
	if (msg->getUser()->getRoom() == nullptr) return false;
	msg->getUser()->leaveRoom();
	return true;
}

void TriviaServer::handleGetUsersInRoom(RecievedMessage * msg)
{
	User* us = msg->getUser();
	int room_id = stoi(msg->getValues()[0]);
	if (getRoomById(room_id) == nullptr) 
		Helper::sendData(msg->getSock(), "1080"); // failed - room not exist or other reason
	
	string users=getRoomById(room_id)->getUsersListMessage();
	Helper::sendData(msg->getSock(), ("108"+users).c_str());
}

void TriviaServer::handleGetRooms(RecievedMessage * msg)
{
	string message = "106";
	string str = to_string(_roomsList.size());

	unsigned int length = str.length();
	for (unsigned int i = 4; i > length; i--)
		str = "0" + str;

	message += str;
	
	

	string len;
	for (std::map<int, Room*>::iterator it = _roomsList.begin(); it != _roomsList.end(); it++)
	{
		str = to_string(it->second->getId());
		length = str.length();
		for (unsigned int i = 4; i > length; i--)
			str = "0" + str;

		message += str;

		len = to_string(it->second->getName().length());
		if (len.length() < 2)
			len = "0" + len;

		message += len + it->second->getName();
	}

	Helper::sendData(msg->getSock(), message);
}

//only this left
void TriviaServer::handleGetBestScores(RecievedMessage * msg)
{
	vector<string> values = _db->getBestScores();
	string str = "124";

	for (int i = 0; i < 3; i++)
	{
		while (values[i * 3 + 0].length() < 2) // username size
			values[i * 3 + 0] = "0" + values[i * 3 + 0];
		str += values[i * 3 + 0];

		str += values[i * 3 + 1]; // username

		while (values[i * 3 + 2].length() < 6) // score
			values[i * 3 + 2] = "0" + values[i * 3 + 2];
		str += values[i * 3 + 2];
	}
	

	msg->getUser()->send(str);
}

void TriviaServer::handleGetPersonalStatus(RecievedMessage * msg)
{
	vector<string> values = _db->getPersonalStatus(msg->getUser()->getUsername());
	string str = "126";
	
	while (values[0].length() < 4) //number of games
		values[0] = "0" + values[0];
	str += values[0];

	while (values[1].length() < 6) //number of right answers
		values[1] = "0" + values[1];
	str += values[1];

	while (values[2].length() < 6) //number of wrong answers
		values[2] = "0" + values[2];
	str += values[2];

	str += (values[3].size() == 1 ? "0" : "") + values[3];
	str += (values[4].size() == 1 ? "0" : "") + values[4];
	msg->getUser()->send(str);
}

void TriviaServer::addRecievedMessage(RecievedMessage * msg)
{
	unique_lock<mutex> locker(_mtxRecievedMessages);
	_queRcvMessages.push(msg);
	locker.unlock();
	cond.notify_one();
}

RecievedMessage* TriviaServer::buildRecieveMessage(SOCKET client_socket, int msgCode)
{
	vector <string> values;
	int length;

	switch (msgCode)
	{
	case 203:
		length = Helper::getIntPartFromSocket(client_socket, 2);
		values.push_back(Helper::getStringPartFromSocket(client_socket, length));
	case 200:
		length = Helper::getIntPartFromSocket(client_socket, 2);
		values.push_back(Helper::getStringPartFromSocket(client_socket, length));
		length = Helper::getIntPartFromSocket(client_socket, 2);
		values.push_back(Helper::getStringPartFromSocket(client_socket, length));
		break;

	case 207:
	case 209:
		values.push_back(Helper::getStringPartFromSocket(client_socket, 4));
		break;

	case 213:
		length = Helper::getIntPartFromSocket(client_socket, 2);
		values.push_back(Helper::getStringPartFromSocket(client_socket, length));
		values.push_back(Helper::getStringPartFromSocket(client_socket, 1));
		values.push_back(Helper::getStringPartFromSocket(client_socket, 2));
		values.push_back(Helper::getStringPartFromSocket(client_socket, 2));
		break;
	
	case 219:
		values.push_back(Helper::getStringPartFromSocket(client_socket, 1));
		values.push_back(Helper::getStringPartFromSocket(client_socket, 2));
		break;

	case 228: //bonus code
		length = Helper::getIntPartFromSocket(client_socket, 4);
		values.push_back(Helper::getStringPartFromSocket(client_socket, length));
		if (1 == 0)
		{
			length = 3;
			values.push_back("hey");
		}
		break;

	case 201:
	case 205:
	case 211:
	case 215:
	case 217:
	case 222:
	case 223:
	case 225:
	case 299:
		return new RecievedMessage(client_socket, msgCode);
		break;

	default:
		return nullptr;
	}

	return new RecievedMessage(client_socket, msgCode, values);
}
