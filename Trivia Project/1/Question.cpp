#include "Question.h"

Question::Question(int id, string question, string correctAnswer, string answer2, string answer3, string answer4):
	_id(id), _question(question)
{
	
	
	_correctAnswerIndex = rand() % 4;
	_answers[_correctAnswerIndex] = correctAnswer;

	int x2, x3, x4;
	do
	{
		x2 = rand() % 4;
	} while (x2 == _correctAnswerIndex);
	_answers[x2] = answer2;

	do
	{
		x3 = rand() % 4;
	} while (x3 == _correctAnswerIndex || x3 == x2);
	_answers[x3] = answer3;

	do
	{
		x4 = rand() % 4;
	} while (x4 == _correctAnswerIndex || x4 == x2 || x4 == x3);
	_answers[x4] = answer4;
}

Question::~Question()
{
}

string Question::getQuestion()
{
	return _question;
}

string * Question::getAnswers()
{
	return _answers;
}

int Question::getCorrectAnswerIndex()
{
	return _correctAnswerIndex;
}

int Question::getId()
{
	return _id;
}
