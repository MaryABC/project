#pragma once
#include"Includes.h"
#include"DataBase.h"

class User;
class DataBase;

class Game
{
public:
	Game(const vector<User*>& players, int questionsNo, DataBase &db);
	~Game();
	void sendQuestionToAllUsers();
	void handleFinishGame();
	void sendFirstQuestion();
	bool handleNextTurn();
	bool handleAnswerFromUser(User* user, int answerNo,int time);
	bool leaveGame(User* currUser);
	void sendListOfUsers(); //this is the third bonus "Know who the opposing players in real time"
	//the message with the list of current players will be sent during the game when someone exits to all other players
	//the messege code is 127  [127 ##(numberOfUsers) ##(username_length) username ##(username_length) username �]

	void handleChat(User* user, string chat_msg);
	//chat during game bonus:
	//messege code 228- client to server, the message that the client sent [228 ####(message length) message]
	//message code 129- server to client, forwording a clients message to all clients 
	//(the client that sent it recieves it too, that's how he knows that the sent was succesful)
	//[129 ##(usernameOfSender_length) username ####(message length) message]

private:
	vector<Question*> _questions;
	vector<User*> _players;
	int _questionsNo;
	int _currQuestionIndex;
	DataBase & _db;
	map<string, int> _results;
	int _currentTurnAnswers;
	int _gameId;
};

