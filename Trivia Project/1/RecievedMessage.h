#pragma once

#include "Includes.h"

class User;

class RecievedMessage
{
public:
	RecievedMessage(SOCKET, int);
	RecievedMessage(SOCKET, int, vector <string>);
	SOCKET getSock();
	User * getUser();
	void setUser(User *);
	int getMessageCode();
	vector <string> & getValues();
	~RecievedMessage();
private:
	User * _user;
	SOCKET _sock;
	int _messageCode;
	vector <string> _values;
};

