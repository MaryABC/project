#include "Room.h"

Room::Room(int id, User * admin, string name, int maxUsers, int questionsNo, int questionTime)
	: _id(id), _admin(admin), _name(name), _maxUsers(maxUsers), _questionsNo(questionsNo), _questionsTime(questionTime)
{
	_users.push_back(admin);
}

Room::~Room()
{
}

int Room::getId()
{
	return _id;
}

string Room::getName()
{
	return _name;
}

vector<User*> Room::getUsers()
{
	return _users;
}

string Room::getUsersAsString(vector<User*> usersList, User * excludeUser)
{
	string users = "";

	for (unsigned int i = 0; i < usersList.size(); i++)
	{
		if (usersList[i] != excludeUser)
			users += usersList[i]->getUsername();
	}

	return users;
}

string Room::getUsersListMessage()
{
	string users = to_string(byte(_users.size()));


	string len;
	for (unsigned int i = 0; i < _users.size(); i++)
	{
		len = to_string(_users[i]->getUsername().length());
		if (len.length() < 2)
			len = "0" + len;

		users += len + _users[i]->getUsername();
	}
	return users;
}

int Room::getQuestionsNo()
{
	return _questionsNo;
}

void Room::sendMessage(User * excludeUser, string message)
{
	try
	{
		for (unsigned int i = 0; i < _users.size(); i++)
		{
			if (_users[i] != excludeUser)
				_users[i]->send(message);
		}
	}
	catch (exception e)
	{

	}
}

void Room::sendMessage(string message)
{
	sendMessage(nullptr, message);
}

bool Room::joinRoom(User * user) 
{
	if (unsigned(_maxUsers) <= _users.size())
	{	
		user->send("1101");
		return false;
	}
	
	_users.push_back(user);
	string str = "1100";
	if (_questionsNo < 10)
		str += "0";
	str += to_string(_questionsNo);
	if (_questionsTime < 10)
		str += "0";
	str += to_string(_questionsTime);
	user->send(str);
	sendMessage("108"+getUsersListMessage());
	return true;
}

void Room::leaveRoom(User * user) // Needs to send a message ** Add later
{
	bool isFound = false;

	for (unsigned int i = 0; i < _users.size() && !isFound; i++)
		if (_users[i] == user)
		{
			isFound = true;			
			_users.erase(_users.begin() + i);
			user->send("1120");
		}
	if (!isFound)
	{
		user->send("116"); //not found. "closing room" code
	}
	else
	{
		sendMessage("108" + getUsersListMessage());
	}
}

int Room::closeRoom(User * user) 
{
	if (user != _admin)
		return -1;

	sendMessage("116");

	for (unsigned int i = 0; i < _users.size(); i++)
	{
		if (_users[i] != _admin)
			_users[i]->clearRoom();
	}

	return _id;
}
