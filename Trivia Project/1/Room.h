#pragma once

#include "Includes.h"

class User;

class Room
{
public:
	Room(int, User *, string, int, int, int);
	~Room();
	int getId();
	string getName();
	vector <User *> getUsers();
	string getUsersAsString(vector<User *>, User *);
	string getUsersListMessage();
	int getQuestionsNo();
	void sendMessage(User *, string);
	void sendMessage(string);
	bool joinRoom(User *);
	void leaveRoom(User *);
	int closeRoom(User *);

private:
	vector <User *> _users;
	User * _admin;
	int _maxUsers;
	int _questionsTime;
	int _questionsNo;
	string _name;
	int _id;
};

