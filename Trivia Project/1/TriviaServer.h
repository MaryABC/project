#pragma once

#define _WINSOCK_DEPRECATED_NO_WARNINGS
#include "Includes.h"
#include <ctime>

class RecievedMessage;
class Room;
class User;
class DataBase;

class TriviaServer
{
public:
	TriviaServer();
	~TriviaServer();
	void serve();
	void bindAndListen();
	void accept();
	void clientHandler(SOCKET client_socket);
	Room* getRoomById(int roomId);
	User* getUserByName(string username);
	User* getUserBySocket(SOCKET client_socket);
	void handleRecievedMessages();
	void safeDeleteUesr(RecievedMessage* msg);
	User* handleSignin(RecievedMessage* msg);
	void handleSignout(RecievedMessage* msg);
	bool handleSignup(RecievedMessage* msg);
	void handleLeaveGame(RecievedMessage* msg);
	void handleStartGame(RecievedMessage* msg);
	void handlePlayerAnswer(RecievedMessage* msg);
	void handleGameChat(RecievedMessage* msg);
	bool handleCreateRoom(RecievedMessage* msg);
	bool handleCloseRoom(RecievedMessage* msg);
	bool handleJoinRoom(RecievedMessage* msg);
	bool handleLeaveRoom(RecievedMessage* msg);
	void handleGetUsersInRoom(RecievedMessage* msg);
	void handleGetRooms(RecievedMessage* msg);
	void handleGetBestScores(RecievedMessage* msg);
	void handleGetPersonalStatus(RecievedMessage* msg);
	void addRecievedMessage(RecievedMessage* msg);
	RecievedMessage* buildRecieveMessage(SOCKET client_socket, int msgCode);

private:
	SOCKET _socket;
	map<SOCKET, User*> _connectedUsers;
	DataBase * _db;
	map<int, Room*> _roomsList;
	condition_variable cond;

	mutex _mtxRecievedMessages;
	queue<RecievedMessage*> _queRcvMessages;

	int _roomIdSequence;
};

