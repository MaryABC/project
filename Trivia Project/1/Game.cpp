#include "Game.h"




Game::Game(const vector<User*>& players, int questionsNo, DataBase &db)
:_db(db)
{
	_gameId = _db.insertNewGame();
	if (_gameId == -1)
		throw exception();
	_currQuestionIndex = 0;
	_questionsNo = questionsNo;
	_questions = _db.initQuestions(_questionsNo);
	_players = players; //should we copy one by one?
	for (unsigned int i = 0; i < _players.size(); i++)
	{
		_results[_players[i]->getUsername()] = 0;
		_players[i]->setGame(this);
		_players[i]->closeRoom();
	}
}

Game::~Game()
{
	for (unsigned int i = 0; i < _questions.size(); i++)
	{
		delete _questions[i];
	}
}

void Game::sendQuestionToAllUsers()
{
	_currentTurnAnswers = 0;
	string str = "118";
	string len_str = to_string(_questions[_currQuestionIndex]->getQuestion().length());
	while (len_str.length() < 3)
		len_str = "0" + len_str;
	str += len_str + _questions[_currQuestionIndex]->getQuestion();
	for (int i = 0; i < 4; i++)
	{
		string len_str = to_string(_questions[_currQuestionIndex]->getAnswers()[i].length());
		while (len_str.length() < 3)
			len_str = "0" + len_str;
		str += len_str + _questions[_currQuestionIndex]->getAnswers()[i];
	}
	for (unsigned int i = 0; i < _players.size(); i++)
	{
		try {
			_players[i]->send(str);
		}
		catch(exception e)
		{ }
	}
}

void Game::handleFinishGame()
{
	_db.updateGameStatus(_gameId);
	string str = "121"+to_string(_players.size()); //or without to_string
	for (unsigned int i = 0; i < _players.size(); i++)
	{
		string len_str = to_string(_players[i]->getUsername().length());
		while (len_str.length() < 2)
			len_str = "0" + len_str;
		string score_str = to_string(_results[_players[i]->getUsername()]);
		while (score_str.length() < 2)
			score_str = "0" + score_str;
		str += len_str + _players[i]->getUsername() + score_str;
	}
	for (unsigned int i = 0; i < _players.size(); i++)
	{
		try {
			_players[i]->send(str);
		}
		catch (exception e)
		{
		}
	}
	for (unsigned int i = 0; i < _players.size(); i++)
	{
		try {
			_players[i]->send(str);
		}
		catch (exception e)
		{
		}
	}
}

void Game::sendFirstQuestion()
{
	sendQuestionToAllUsers();
}

bool Game::handleNextTurn()
{
	if (_players.size() == 0)
	{
		handleFinishGame();
		return false;
	}
	if (_currentTurnAnswers != _players.size()) 
		return true;
	if (_currQuestionIndex == _questionsNo - 1)
	{
		handleFinishGame();
		return false;
	}
	_currQuestionIndex++;
	sendQuestionToAllUsers();
	return true;
}

bool Game::handleAnswerFromUser(User * user, int answerNo, int time)
{
	if (answerNo-1 == _questions[_currQuestionIndex]->getCorrectAnswerIndex())
		_results[user->getUsername()] += 1;
	_db.addAnswerToPlayer(_gameId, user->getUsername(), _questions[_currQuestionIndex]->getId(), (answerNo == 5 ? "" : _questions[_currQuestionIndex]->getAnswers()[answerNo - 1]), (answerNo - 1 == _questions[_currQuestionIndex]->getCorrectAnswerIndex()), time);
	user->send("120"+ to_string(answerNo - 1 == _questions[_currQuestionIndex]->getCorrectAnswerIndex()? 1 : 0) );
	_currentTurnAnswers++;
	return handleNextTurn();
}

bool Game::leaveGame(User * currUser)
{
	int user_index = -1;
	for (unsigned int i = 0; i < _players.size() && user_index == -1; i++)
	{
		if (_players[i] == currUser)
			user_index = i;
	}
	_players.erase(_players.begin() + user_index);
	if (_players.size() == 0)
		return false;
	sendListOfUsers(); //bonus related
	handleNextTurn();
	return true;
}

void Game::sendListOfUsers() //this is the third bonus "Know who the opposing players in real time"
{
	string users = "127";


	string len;
	len = to_string(byte(_players.size()));
	if (len.length() < 2)
		len = "0" + len;
	users += len;

	for (unsigned int i = 0; i < _players.size(); i++)
	{
		len = to_string(_players[i]->getUsername().length());
		if (len.length() < 2)
			len = "0" + len;

		users += len + _players[i]->getUsername();
	}
	for (unsigned int i = 0; i < _players.size(); i++)
	{
		try {
			_players[i]->send(users);
		}
		catch (exception e)
		{
		}
	}
}

void Game::handleChat(User * user, string chat_msg)
{
	string str = "129";

	string len = to_string(user->getUsername().length());
	if (len.length() < 2)
		len = "0" + len;
	str += len + user->getUsername();

	len = to_string(chat_msg.length());
	while (len.length() < 4)
		len = "0" + len;
	str += len + chat_msg;

	for (unsigned int i = 0; i < _players.size(); i++)
	{
		try {
			_players[i]->send(str);
		}
		catch (exception e)
		{
		}
	}
}
