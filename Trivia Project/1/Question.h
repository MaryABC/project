#pragma once
#include<string>
using namespace std;
class Question
{
public:
	Question(int id, string question, string correctAnswer, string answer2, string answer3, string answer4);
	~Question();
	string getQuestion();
	string* getAnswers();
	int getCorrectAnswerIndex();
	int getId();

private:
	int _id;
	string _question;
	string _answers[4];
	int _correctAnswerIndex;

};

