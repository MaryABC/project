#pragma once

#include <iostream>
#include "Includes.h"
#include <unordered_map>

class Game;
class DataBase
{

public:
	DataBase(string dbName = "database.db");
	~DataBase();
	bool isUserExist(string);
	bool addNewUser(string, string, string); // string username, string password, string email
	bool isUserAndPassMatch(string, string); // string username, string password
	vector<Question *> initQuestions(int); // int questionsNo
	int insertNewGame();
	bool updateGameStatus(int);
	bool addAnswerToPlayer(int, string, int, string, bool, int); // int gameId, string username, int questionId, string answer, bool isCorrect, int answerTime
	vector <string> getPersonalStatus(string);
	vector<string> DataBase::getBestScores();

	static int callbackCount(void *, int, char **, char **);
	static int callbackQuestions(void *, int, char **, char **);
	static int callbackBestScores(void *, int, char **, char **);
	static int callbackPersonalStatus(void *, int, char **, char **);

private:
	bool openDB(string);
	bool execDB(string, int(*callback)(void *, int, char **, char **) = NULL);

//	int callback(void *, int, char **, char **);
//	void clearTable();

	sqlite3 * _db;
	char * _zErrMsg = 0;

//	unordered_map<string, vector<string>> _results;
};

