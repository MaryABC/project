#pragma comment(lib, "ws2_32.lib") 
#include "Helper.h"
#include "RecievedMessage.h"
#include "Room.h"
#include "sqlite3.h"
#include "TriviaServer.h"
#include "User.h"
#include "Validator.h"
#include "Question.h"
#include "Game.h"
//#include "DataBase.h" too many includes error

#include <string>
#include <vector>
#include <map>
#include <mutex>
#include <condition_variable>
#include <WinSock2.h>
#include <Windows.h>
#include <queue>
#include <thread>

using namespace std;