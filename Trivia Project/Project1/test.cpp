#include "sqlite3.h"
#include <unordered_map>
#include <vector>
#include <string>
#include <iostream>

using namespace std;

unordered_map<string, vector<string>> _results;
int callback(void* notUsed, int argc, char** argv, char** azCol);
bool execDB(string op, int(*callback)(void *, int, char **, char **) = NULL);
sqlite3 * _db;
char * _zErrMsg = 0;

bool isUserExist(string username)
{
	string op = "select * from t_users where username == '" + username + "';";
	if (!execDB(op, callback))
		return false;

	if (_results.size() != 0)
	{
		auto iter = _results.end();
		iter--;
		int size = iter->second.size();
		auto it = _results.begin();

		if (size == 1 && it->second[0].compare(username) == 0)
			return true;
	}
	return false;
}



int callback(void* notUsed, int argc, char** argv, char** azCol)
{
	int i;

	for (i = 0; i < argc; i++)
	{
		auto it = _results.find(azCol[i]);
		if (it != _results.end())
		{
			it->second.push_back(argv[i]);
		}
		else
		{
			pair<string, vector<string>> p;
			p.first = azCol[i];
			p.second.push_back(argv[i]);
			_results.insert(p);
		}
	}

	return 0;
}

bool openDB(string dbName)
{
	int rc = sqlite3_open(dbName.c_str(), &_db);
	if (rc)
	{
		//		cout << "Can't open database: " << sqlite3_errmsg(_db) << endl;
		sqlite3_close(_db);
		return false;
	}
	return true;
}

bool execDB(string op, int(*callback)(void *, int, char **, char **))
{
	int rc = sqlite3_exec(_db, op.c_str(), callback, 0, &_zErrMsg);
	if (rc != SQLITE_OK)
	{
		//		cout << "SQL error: " << _zErrMsg << endl;
		sqlite3_free(_zErrMsg);
		return false;
	}
	return true;
}

int main()
{
	openDB("database.db");

	if (isUserExist("igal"))
		cout << "igal" << endl;

	sqlite3_close(_db);

	system("pause");
	return 0;
}